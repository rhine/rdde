#include <Rcpp.h>
using namespace Rcpp;

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <tchar.h>
#include <ddeml.h>
#include "src/DdeCallback.h"

std::string DDERequest(DWORD idInst, HCONV hConv, LPSTR szItem)
{
  HSZ hszItem = DdeCreateStringHandle(idInst, szItem, 0);
  HDDEDATA hData = DdeClientTransaction(NULL, 0, hConv, hszItem, CF_TEXT, XTYP_REQUEST, 5000, NULL);
  TCHAR szResult[255];
  DdeGetData(hData, (unsigned char *)szResult, 255, 0);
  return std::string(szResult);
}

// [[Rcpp::export]]
std::string testMe()
{
  char szApp[] = "EXCEL";
  char szTopic[] = "[DdeTest.xlsx]Sheet1";
  char szItem[] = "R1C1";

  //DDE Initialization
  DWORD idInst = 0;
  UINT iReturn;
  iReturn = DdeInitialize(&idInst, (PFNCALLBACK)DdeCallback,
                          APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0);
  if (iReturn == 0) {} else {}
  //DDE Connect to Server using given AppName and topic.
  HSZ hszApp, hszTopic;
  HCONV hConv;
  hszApp = DdeCreateStringHandle(idInst, szApp, 0);
  hszTopic = DdeCreateStringHandle(idInst, szTopic, 0);
  hConv = DdeConnect(idInst, hszApp, hszTopic, NULL);
  DdeFreeStringHandle(idInst, hszApp);
  DdeFreeStringHandle(idInst, hszTopic);

  std::string result;
  result = DDERequest(idInst, hConv, szItem);

  //DDE Disconnect and Uninitialize.
  DdeDisconnect(hConv);
  DdeUninitialize(idInst);

  return result;
}

// [[Rcpp::export]]
std::string testMeWithArgs(std::string app, std::string topic, std::string item)
{

  const char *szApp = app.data();
  const char *szTopic = topic.data();
  char *szItem = (char *) item.data();

  //DDE Initialization
  DWORD idInst = 0;
  UINT iReturn;
  iReturn = DdeInitialize(&idInst, (PFNCALLBACK)DdeCallback,
                          APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0);
  if (iReturn == 0) {} else {}
  //DDE Connect to Server using given AppName and topic.
  HSZ hszApp, hszTopic;
  HCONV hConv;
  hszApp = DdeCreateStringHandle(idInst, szApp, 0);
  hszTopic = DdeCreateStringHandle(idInst, szTopic, 0);
  hConv = DdeConnect(idInst, hszApp, hszTopic, NULL);
  DdeFreeStringHandle(idInst, hszApp);
  DdeFreeStringHandle(idInst, hszTopic);

  std::string result;
  result = DDERequest(idInst, hConv, szItem);

  //DDE Disconnect and Uninitialize.
  DdeDisconnect(hConv);
  DdeUninitialize(idInst);

  return result;
}



